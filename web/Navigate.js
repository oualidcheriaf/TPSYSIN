//	-------------------------------------------------------------
//	AJAX functions
//
//	Jean-Yves MARTIN
//	Ecole Centrale Nantes
//	(c)2007 Ecole Centrale de Nantes
//	-------------------------------------------------------------


//	-------------------------------------------------------------
function getXMLHTTP() {
    var xhr = null;

    if (window.XMLHttpRequest)
    // Firefox et autres
        xhr = new XMLHttpRequest();
    else if (window.ActiveXObject) {
        // Internet Explorer
        try {
            xhr = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
            try {
                xhr = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e1) {
            }
        }
    }
    else {
        // XMLHttpRequest non support� par le navigateur
    }
    return xhr;
}

function validate(page) {
    var xhr = getXMLHTTP();
    if (xhr != null) {
        xhr.open("POST", "controle", false);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.send("page=" + page);

        console.log(xhr.responseText)
    }
    return false;
}

function loadUser() {

    return false;
}

function loadUserCursus() {

    return false;
}
