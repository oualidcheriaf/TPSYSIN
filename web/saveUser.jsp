<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="fr-fr">
<head>
    <meta charset="UTF-8"/>
    <title>.Inscription</title>
    <link href="feuillestyle.css" rel="stylesheet" type="text/css"/>
</head>
<%@page import="org.centrale.intec.Personne" %>
<% Personne aUser = new Personne((String) request.getAttribute("userFileID"));%>
<body>
<h2>Enregistrement des données</h2>
<p>Enregistrement du dossier <%=aUser.getUserFileID() %>
</p>
<p>Nom : <%=aUser.getUserLastName()%>
</p>
<p>Prénom : <%=aUser.getUserFirstName()%>
</p>
<p>Date de naissance : <%=aUser.getUserDisplayableBirthDay() %>
</p>
<p>Sexe : <%=aUser.getUserTypeStr()%>
</p>
</body>
</html>

