/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.sysin;

import org.centrale.intec.Personne;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;

/**
 * @author kwyhr
 */
public class Inscription {

    private Connection laConnexion;
    private Personne personne;
    private Cursus cursus;
    private Annee annee;

    /**
     * Build from elements
     *
     * @param personne
     * @param cursus
     * @param annee
     */
    public Inscription(Personne personne, Cursus cursus, Annee annee) {
        this.personne = personne;
        this.cursus = cursus;
        this.annee = annee;
    }

    /**
     * Build from request
     *
     * @param request
     */
    public Inscription(HttpServletRequest request) {
        personne = new Personne(request);
        cursus = new Cursus(request);
        annee = new Annee(request);
    }

    /**
     * Build from cursusId
     *
     * @param personneId
     * @param cursusId
     * @param anneeId
     */
    public Inscription(int personneId, int cursusId, int anneeId) {
        laConnexion = DataBase.getConnexion();
        if (laConnexion != null) {
            try {
                PreparedStatement query = laConnexion.prepareStatement("SELECT * FROM Inscription NATURAL JOIN Personne NATURAL JOIN Cursus NATURAL JOIN Annee NATURAL LEFT OUTER JOIN Sexe WHERE Personne_ID=? AND Cursus_ID=? AND Annee_ID=?");
                query.setInt(1, personneId);
                query.setInt(2, cursusId);
                query.setInt(3, anneeId);
                ResultSet res = query.executeQuery();
                if (res.next()) {
                    this.personne = new Personne(res);
                    this.cursus = new Cursus(res);
                    this.annee = new Annee(res);
                }
                laConnexion.close();

            } catch (SQLException ex) {
                Logger.getLogger(Annee.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        laConnexion = null;
    }

    /**
     * Build from cursusId
     *
     * @param personneId
     * @return
     */
    public static List<Inscription> getInscriptions(String aUserFileID) {
        Connection laConnexion = DataBase.getConnexion();
        List<Inscription> theList = new ArrayList<Inscription>();
        if (laConnexion != null) {
            try {
                PreparedStatement query = laConnexion.prepareStatement("SELECT * FROM Inscription NATURAL JOIN Personne NATURAL JOIN Cursus NATURAL JOIN Annee NATURAL LEFT OUTER JOIN Sexe WHERE Personne_NumeroDossier=?");
                query.setString(1, aUserFileID);
                ResultSet res = query.executeQuery();
                while (res.next()) {
                    Personne personne = new Personne(res);
                    Cursus cursus = new Cursus(res);
                    Annee annee = new Annee(res);
                    Inscription anInscription = new Inscription(personne, cursus, annee);
                    theList.add(anInscription);
                }
                laConnexion.close();

            } catch (SQLException ex) {
                Logger.getLogger(Annee.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        laConnexion = null;
        return theList;
    }

    /**
     * Save user
     */
    public void save() {
        laConnexion = DataBase.getConnexion();
        if (laConnexion != null) {
            try {
                StringBuilder qString = new StringBuilder();

                qString.append("INSERT INTO Inscription(");
                qString.append("Personne_ID");
                qString.append(", Cursus_ID");
                qString.append(", AnneeID");
                qString.append(") VALUES (?, ?, ?)");

                PreparedStatement query = laConnexion.prepareStatement(qString.toString());
                query.setInt(1, this.personne.getUserId());
                query.setInt(2, this.cursus.getCursusId());
                query.setInt(3, this.annee.getAnneeId());
                query.executeUpdate();

                laConnexion.close();

            } catch (SQLException ex) {
                Logger.getLogger(Annee.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        laConnexion = null;
    }

    public Personne getPersonne() {
        return personne;
    }

    public void setPersonne(Personne personne) {
        this.personne = personne;
    }

    public Cursus getCursus() {
        return cursus;
    }

    public void setCursus(Cursus cursus) {
        this.cursus = cursus;
    }

    public Annee getAnnee() {
        return annee;
    }

    public void setAnnee(Annee annee) {
        this.annee = annee;
    }
}
