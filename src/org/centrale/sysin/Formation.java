/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.sysin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;

/**
 * @author kwyhr
 */
public class Formation {

    private Connection laConnexion;
    private Cursus cursus;
    private Annee annee;

    /**
     * Build from request
     *
     * @param request
     */
    public Formation(HttpServletRequest request) {
        cursus = new Cursus(request);
        annee = new Annee(request);
    }

    /**
     * Build from cursusId
     *
     * @param cursusId
     * @param anneeId
     */
    public Formation(int cursusId, int anneeId) {
        laConnexion = DataBase.getConnexion();
        if (laConnexion != null) {
            try {
                PreparedStatement query = laConnexion.prepareStatement("SELECT * FROM Formation NATURAL JOIN Cursus NATURAL JOIN Annee WHERE Cursus_ID=? AND Annee_ID=?");
                query.setInt(1, cursusId);
                query.setInt(2, anneeId);
                ResultSet res = query.executeQuery();
                if (res.next()) {
                    this.cursus = new Cursus(res);
                    this.annee = new Annee(res);
                }
                laConnexion.close();

            } catch (SQLException ex) {
                Logger.getLogger(Annee.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        laConnexion = null;
    }

    /**
     * Save user
     */
    public void save() {
        laConnexion = DataBase.getConnexion();
        if (laConnexion != null) {
            try {
                StringBuilder qString = new StringBuilder();

                qString.append("INSERT INTO Formation(");
                qString.append("Cursus_ID");
                qString.append(", Annee_ID");
                qString.append(") VALUES (?, ?)");

                PreparedStatement query = laConnexion.prepareStatement(qString.toString());
                query.setInt(1, this.cursus.getCursusId());
                query.setInt(2, this.annee.getAnneeId());
                query.executeUpdate();

                laConnexion.close();

            } catch (SQLException ex) {
                Logger.getLogger(Annee.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        laConnexion = null;
    }

    public Cursus getCursus() {
        return cursus;
    }

    public void setCursus(Cursus cursus) {
        this.cursus = cursus;
    }

    public Annee getAnnee() {
        return annee;
    }

    public void setAnnee(Annee annee) {
        this.annee = annee;
    }
}
