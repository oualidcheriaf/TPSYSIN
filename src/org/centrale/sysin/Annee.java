/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.sysin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;

/**
 * @author kwyhr
 */
public class Annee {

    private Connection laConnexion;
    private int anneeId;
    private int anneeUniversitaire;
    private String anneeLibelle;

    /**
     * Build from request
     *
     * @param request
     */
    public Annee(HttpServletRequest request) {
        anneeId = Utilities.getRequestInteger(request, "anneeId");
        anneeUniversitaire = Utilities.getRequestInteger(request, "anneeUniversitaire");
        anneeLibelle = Utilities.getRequestString(request, "anneeLibelle").trim();
    }

    /**
     * Build from anneeID
     *
     * @param anneeId
     */
    public Annee(int anneeId) {
        laConnexion = DataBase.getConnexion();
        if (laConnexion != null) {
            try {
                PreparedStatement query = laConnexion.prepareStatement("SELECT * FROM Annee WHERE Annee_ID=?");
                query.setInt(1, anneeId);
                ResultSet res = query.executeQuery();
                if (res.next()) {
                    this.anneeId = res.getInt("Annee_ID");
                    this.anneeUniversitaire = res.getInt("Annee_Universitaire");
                    this.anneeLibelle = res.getString("Annee_Libelle");
                }
                laConnexion.close();

            } catch (SQLException ex) {
                Logger.getLogger(Annee.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        laConnexion = null;
    }

    /**
     * Build from anneeID
     *
     * @param res
     */
    public Annee(ResultSet res) {
        if (res != null) {
            try {
                this.anneeId = res.getInt("Annee_ID");
                this.anneeUniversitaire = res.getInt("Annee_Universitaire");
                this.anneeLibelle = res.getString("Annee_Libelle");
            } catch (SQLException ex) {
                Logger.getLogger(Annee.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Save annee
     *
     * @return
     */
    public int save() {
        laConnexion = DataBase.getConnexion();
        if (laConnexion != null) {
            try {
                StringBuilder qString = new StringBuilder();

                if (anneeId <= 0) {
                    qString.append("INSERT INTO Annee(");
                    qString.append("Annee_Universitaire");
                    qString.append(", Annee_Libelle");
                    qString.append(") VALUES (?, ?)");
                } else {
                    qString.append("UPDATE Annee SET ");
                    qString.append("Annee_Universitaire=?");
                    qString.append(", Annee_Libelle=?");
                    qString.append(" WHERE Annee_ID=?");
                }

                PreparedStatement query = laConnexion.prepareStatement(qString.toString());
                query.setInt(1, this.anneeUniversitaire);
                query.setString(2, this.anneeLibelle);
                if (anneeId > 0) {
                    query.setInt(3, this.anneeId);
                }
                query.executeUpdate();

                laConnexion.close();
            } catch (SQLException ex) {
                Logger.getLogger(Annee.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        laConnexion = null;

        return anneeId;
    }

    /**
     * save User in database
     */
    private void saveAnnee() {
    }

    public int getAnneeId() {
        return anneeId;
    }

    public void setAnneeId(int anneeId) {
        this.anneeId = anneeId;
    }

    public int getAnneeUniversitaire() {
        return anneeUniversitaire;
    }

    public void setAnneeUniversitaire(int anneeUniversitaire) {
        this.anneeUniversitaire = anneeUniversitaire;
    }

    public String getAnneeLibelle() {
        return anneeLibelle;
    }

    public void setAnneeLibelle(String anneeLibelle) {
        this.anneeLibelle = anneeLibelle;
    }

}
