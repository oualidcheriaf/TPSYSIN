/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.sysin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;

/**
 * @author kwyhr
 */
public class Cursus {

    private Connection laConnexion;
    private int cursusId;
    private String cursusLibelle;

    /**
     * Build from request
     *
     * @param request
     */
    public Cursus(HttpServletRequest request) {
        cursusId = Utilities.getRequestInteger(request, "cursusId");
        cursusLibelle = Utilities.getRequestString(request, "cursusLibelle").trim();
    }

    /**
     * Build from cursusId
     *
     * @param cursusId
     */
    public Cursus(int cursusId) {
        laConnexion = DataBase.getConnexion();
        if (laConnexion != null) {
            try {
                PreparedStatement query = laConnexion.prepareStatement("SELECT * FROM Cursus WHERE Cursus_ID=?");
                query.setInt(1, cursusId);
                ResultSet res = query.executeQuery();
                if (res.next()) {
                    this.cursusId = res.getInt("Cursus_ID");
                    this.cursusLibelle = res.getString("Cursus_Libelle");
                }
                laConnexion.close();

            } catch (SQLException ex) {
                Logger.getLogger(Annee.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        laConnexion = null;
    }

    /**
     * Build from res
     *
     * @param res
     */
    public Cursus(ResultSet res) {
        if (res != null) {
            try {
                this.cursusId = res.getInt("Cursus_ID");
                this.cursusLibelle = res.getString("Cursus_Libelle");
            } catch (SQLException ex) {
                Logger.getLogger(Annee.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Save cursus
     *
     * @return
     */
    public int save() {
        laConnexion = DataBase.getConnexion();
        if (laConnexion != null) {
            try {
                StringBuilder qString = new StringBuilder();

                if (cursusId <= 0) {
                    qString.append("INSERT INTO Cursus(");
                    qString.append("Cursus_Libelle");
                    qString.append(") VALUES (?)");
                } else {
                    qString.append("UPDATE Cursus SET ");
                    qString.append("Cursus_Libelle=?");
                    qString.append(" WHERE Cursus_ID=?");
                }

                PreparedStatement query = laConnexion.prepareStatement(qString.toString());
                query.setString(1, this.cursusLibelle);
                if (cursusId > 0) {
                    query.setInt(2, this.cursusId);
                }
                query.executeUpdate();

                laConnexion.close();

            } catch (SQLException ex) {
                Logger.getLogger(Annee.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        laConnexion = null;

        return cursusId;
    }

    public int getCursusId() {
        return cursusId;
    }

    public void setCursusId(int cursusId) {
        this.cursusId = cursusId;
    }

    public String getCursusLibelle() {
        return cursusLibelle;
    }

    public void setCursusLibelle(String cursusLibelle) {
        this.cursusLibelle = cursusLibelle;
    }

}
