/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.sysin;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author kwyhr
 */
public class DataBase {

    private static boolean inited = false;
    private static String dbURL;
    private static String dbLogin;
    private static String dbPassword;

    public static void init() {
        if (!inited) {
            inited = true;
            try {
                ResourceBundle res = ResourceBundle.getBundle(DataBase.class.getPackage().getName() + ".conf");

                String jdbcDriver = res.getString("driver");

                dbURL = res.getString("protocol") + "://" + res.getString("dbHost") + "/" + res.getString("dbName");
                dbLogin = res.getString("dbUser");
                dbPassword = res.getString("dbPass");

                Class.forName(jdbcDriver);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(DataBase.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                Logger.getLogger(DataBase.class.getName()).log(Level.INFO, null, ex);
            }
        }
    }

    public static Connection getConnexion() {
        Connection returnedValue = null;
        init();
        try {
            returnedValue = DriverManager.getConnection(dbURL, dbLogin, dbPassword);
        } catch (SQLException ex) {
            Logger.getLogger(DataBase.class.getName()).log(Level.SEVERE, null, ex);
        }
        return returnedValue;
    }
}
