package org.centrale.sysin;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * @author kwyhr
 */
public class Utilities {

    /**
     * Random generator
     *
     * @param max
     * @return
     */
    public static int getRandomValue(int max) {
        return (int) (Math.random() * max);
    }

    /**
     * convert DATE to standart string
     *
     * @param uneDateStr
     * @return
     */
    public static String convertDate(String uneDateStr, String entryFormat, String exitFormat) {
        String returnedValue = "";
        SimpleDateFormat sdf1 = new SimpleDateFormat(entryFormat);
        Date uneDate = null;
        try {
            uneDate = sdf1.parse(uneDateStr);
        } catch (ParseException ex) {
            Logger.getLogger(Utilities.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (uneDate != null) {
            SimpleDateFormat sdf2 = new SimpleDateFormat(exitFormat);
            returnedValue = sdf2.format(uneDate);
        }

        return returnedValue;
    }

    /**
     * Check if a String is a Date and returns the Date, null otherwise
     *
     * @param aDate
     * @return
     */
    public static Date isDate(String aDate) {
        Date returnedValue = null;
        if (aDate != null) {
            if (!aDate.equals("")) {
                try {
                    SimpleDateFormat aFormater = new SimpleDateFormat("dd/MM/yyyy");
                    returnedValue = aFormater.parse(aDate);
                } catch (ParseException ex) {
                }
                if (returnedValue == null) {
                    try {
                        SimpleDateFormat aFormater = new SimpleDateFormat("yyyy-MM-dd");
                        returnedValue = aFormater.parse(aDate);
                    } catch (ParseException ex) {
                    }
                }
                if (returnedValue == null) {
                    try {
                        SimpleDateFormat aFormater = new SimpleDateFormat("yyyy-dd-MM");
                        returnedValue = aFormater.parse(aDate);
                    } catch (ParseException ex) {
                        Logger.getLogger(Utilities.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

                if (returnedValue != null) {
                    Calendar aCalendar = Calendar.getInstance();
                    aCalendar.setTime(returnedValue);
                    if (aCalendar.get(Calendar.YEAR) < 1000) {
                        aCalendar.add(Calendar.YEAR, 1900);
                        returnedValue = aCalendar.getTime();
                    }
                }
            }
        }

        return returnedValue;
    }

    /**
     * Get string from HTTP request parameter
     *
     * @param request
     * @param name
     * @return
     */
    public static String getRequestString(HttpServletRequest request, String name) {
        String value = request.getParameter(name);
        if (value != null) {
            return value;
        } else {
            return "";
        }
    }

    /**
     * Get integer from HTTP request parameter
     *
     * @param request
     * @param name
     * @return
     */
    public static int getRequestInteger(HttpServletRequest request, String name) {
        String value = request.getParameter(name);
        if (value != null) {
            if (!value.equals("")) {
                return Integer.parseInt(value);
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

}
