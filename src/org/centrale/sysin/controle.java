package org.centrale.sysin;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "controle", urlPatterns = {"/controle"})
public class controle extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        response.setHeader("Content-Type", "text/html");
        String pageCalled = request.getParameter("page");
        String xml;
        System.out.println(34);
        xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
        if (pageCalled != null) {
            if (pageCalled.equals("Ident")) {
                out.print(xml + "<data>ident.html</data>");
            }
            if (pageCalled.equals("Cursus")) {
                out.print(xml + "<data>cursus.html</data>");
            }
        }
        System.out.println(out.toString());

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Dispatcher.callJSP("index.html", request, response);

    }
}
