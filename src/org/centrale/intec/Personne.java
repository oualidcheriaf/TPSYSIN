/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.intec;

import org.centrale.sysin.DataBase;
import org.centrale.sysin.Utilities;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;

/**
 * @author kwyhr
 */
public class Personne {

    private Connection laConnexion;
    private int userId;
    private String userFileID;
    private String userLastName;
    private String userFirstName;
    private int userType;
    private String userTypeStr;
    private String userBirthDay;

    /**
     * Build from request
     *
     * @param request
     */
    public Personne(HttpServletRequest request) {
        userId = Utilities.getRequestInteger(request, "userId");
        userLastName = Utilities.getRequestString(request, "nom").trim();
        userFirstName = Utilities.getRequestString(request, "prenom").trim();
        userType = Utilities.getRequestInteger(request, "sexe");
        userTypeStr = "";
        userBirthDay = request.getParameter("naissance").trim();
        if (Utilities.isDate(userBirthDay) == null) {
            userBirthDay = "";
        }
        userFileID = Utilities.getRequestString(request, "dossier").trim();
    }

    /**
     * Build from FileID
     *
     * @param aUserFileID
     */
    public Personne(String aUserFileID) {
        laConnexion = DataBase.getConnexion();
        if (laConnexion != null) {
            try {
                PreparedStatement query = laConnexion.prepareStatement("SELECT * FROM Personne NATURAL LEFT OUTER JOIN Sexe WHERE Personne_NumeroDossier=?");
                query.setString(1, aUserFileID);
                ResultSet res = query.executeQuery();
                if (res.next()) {
                    this.userFileID = res.getString("Personne_NumeroDossier");
                    this.userLastName = res.getString("Personne_Nom");
                    this.userFirstName = res.getString("Personne_Prenom");
                    String aDate = res.getString("Personne_DateNaissance");
                    if (aDate == null) {
                        this.userBirthDay = "";
                    } else if (aDate.equals("")) {
                        this.userBirthDay = "";
                    } else {
                        this.userBirthDay = Utilities.convertDate(aDate, "yyyy-MM-dd", "dd/MM/yyyy");
                    }
                    this.userType = res.getInt("Sexe_ID");
                    this.userTypeStr = res.getString("Sexe_Libelle");
                }
                laConnexion.close();

            } catch (SQLException ex) {
                Logger.getLogger(Personne.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        laConnexion = null;
    }

    /**
     * Build from FileID
     *
     * @param res
     */
    public Personne(ResultSet res) {
        if (res != null) {
            try {
                this.userId = res.getInt("Personne_ID");
                this.userLastName = res.getString("Personne_Nom");
                this.userFirstName = res.getString("Personne_Prenom");
                String aDate = res.getString("Personne_DateNaissance");
                if (aDate == null) {
                    this.userBirthDay = "";
                } else if (aDate.equals("")) {
                    this.userBirthDay = "";
                } else {
                    this.userBirthDay = Utilities.convertDate(aDate, "yyyy-MM-dd", "dd/MM/yyyy");
                }
                this.userFileID = res.getString("Personne_NumeroDossier");
                this.userType = res.getInt("Sexe_ID");
                this.userTypeStr = res.getString("Sexe_Libelle");
            } catch (SQLException ex) {
                Logger.getLogger(Personne.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Save user
     *
     * @return
     */
    public String save() {
        laConnexion = DataBase.getConnexion();
        if (laConnexion != null) {
            try {
                if (userFileID.equals("")) {
                    generateUserFileID();
                }
                saveUser();
                laConnexion.close();

            } catch (SQLException ex) {
                Logger.getLogger(Personne.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        laConnexion = null;

        return userFileID;
    }

    /**
     * Generate a randon FileID
     */
    private void generateUserFileID() {
        boolean isAlreadyUsed = true;
        while (isAlreadyUsed) {
            // Generate new code
            userFileID = "";
            for (int i = 0; i < 6; i++) {
                if ((i == 1) || (i == 2)) {
                    userFileID += (char) (48 + Utilities.getRandomValue(10));
                } else {
                    userFileID += (char) (65 + Utilities.getRandomValue(26));
                }
            }
            if (!checkUserFileID()) {
                isAlreadyUsed = false;
            }
        }
    }

    /**
     * check if the FileID is in the database
     *
     * @return isInDatabase
     */
    private boolean checkUserFileID() {
        boolean isInDatabase = false;

        try {
            PreparedStatement query = laConnexion.prepareStatement("SELECT * FROM Personne WHERE Personne_NumeroDossier=?");
            query.setString(1, userFileID);
            ResultSet res = query.executeQuery();
            if (res.next()) {
                isInDatabase = true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Personne.class.getName()).log(Level.SEVERE, null, ex);
        }

        return isInDatabase;
    }

    /**
     * save Personne in database
     */
    private void saveUser() {
        StringBuilder qString = new StringBuilder();
        boolean isInDatabase = checkUserFileID();

        if (!isInDatabase) {
            qString.append("INSERT INTO Personne(");
            qString.append("Personne_Nom");
            qString.append(", Personne_Prenom");
            if (!userBirthDay.equals("")) {
                qString.append(", Personne_DateNaissance");
            }
            if (userType > 0) {
                qString.append(", Sexe_ID");
            }
            qString.append(") VALUES (?, ?");
            if (!userBirthDay.equals("")) {
                qString.append(", CAST(? AS DATE)");
            }
            if (userType > 0) {
                qString.append(", ?");
            }
            qString.append(")");
        } else {
            qString.append("UPDATE Personne SET ");
            qString.append("Personne_Nom=?");
            qString.append(", Personne_Prenom=?");
            if (!userBirthDay.equals("")) {
                qString.append(", Personne_DateNaissance=CAST(? AS DATE)");
            } else {
                qString.append(", Personne_DateNaissance=NULL");
            }
            if (userType > 0) {
                qString.append(", Sexe_ID=?");
            } else {
                qString.append(", Sexe_ID=NULL");
            }
            qString.append(" WHERE Personne_NumeroDossier=?");
        }

        try {
            PreparedStatement query = laConnexion.prepareStatement(qString.toString());
            int ind = 1;
            query.setString(ind, this.userLastName);
            ind++;
            query.setString(ind, this.userFirstName);
            ind++;
            if (!userBirthDay.equals("")) {
                query.setString(ind, Utilities.convertDate(this.userBirthDay, "dd/MM/yyyy", "yyyy-MM-dd"));
                ind++;
            }
            if (userType > 0) {
                query.setInt(ind, this.userType);
                ind++;
            }
            if (isInDatabase) {
                query.setString(ind, userFileID);
            } else {
                ResultSet generatedKeys = query.getGeneratedKeys();
                if (generatedKeys.next()) {
                    this.userId = generatedKeys.getInt(1);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(Personne.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserFileID() {
        return userFileID;
    }

    public String getUserLastName() {
        return userLastName;
    }

    public String getUserFirstName() {
        return userFirstName;
    }

    public int getUserType() {
        return userType;
    }

    public String getUserTypeStr() {
        return userTypeStr;
    }

    public String getUserBirthDay() {
        return userBirthDay;
    }

    public String getUserDisplayableBirthDay() {
        return userBirthDay;
    }
}
